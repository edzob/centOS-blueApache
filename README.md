# CentOS blueApache
This repository is a simple centOS container.   
* CentOS Base install 
* updated via yum update.
* Apache (only) installed.
* run a harding script on the apache config files.

**Table of Contents** 
- [CentOS blueApache](#centOS-blueapache)
- [todo](#todo)
- [TL;DR](#tl;dr)
- [On Hardening](#on-hardening)
  - [Goal](#goal)
    - [todo after hardening apache](#todo-after-hardening-apache)
    - [Blue Team, Red Team, Purple team](#blue-team,-red-team,-purple-team)
  - [Approach to hardening / configuration management.](#approach-to-hardening-/-configuration-management.)
    - [Reason for configuration management](#reason-for-configuration-management)
    - [Approaches for configuration management](#approaches-for-configuration-management)
      - [All options have in common](#all-options-have-in-common)
      - [Option 1: Change default config files](#option-1:-change-default-config-files)
        - [Upside change](#upside-change)
        - [Downside change](#downside-change)
      - [Option 2: Replace default config files](#option-2:-replace-default-config-files)
        - [Upside replace](#upside-replace)
        - [Downside replace](#downside-replace)
      - [Option 3: Keep configuration in sync with cm tooling](#option-3:-Keep-configuration-in-sync-with-cm-tooling)
        - [Upside:](#upside)
        - [Downside:](#downside) 
        - [Summary](#summary)
    - [Advice for hardening](#advice-for-hardening)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Run it](#run-it)
- [Built With](#built-with)
  - [Devices](#devices)
  - [CodeEditor](#codeeditor)
  - [Programming Language(s)](#programming-language(s))
  - [Tools](#tools)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)
  - [Main inspiration on the script approach](#main-inspiration-on-the-script-approach)
  - [Inspiration on Apache Hardening Configuration rules](#inspiration-on-apache-hardening-configuration-rules)
  - [Inspiration on HTTTP Headers](#inspiration-on-http-headers)
  - [Future inspiration on mod_security](#future-inspiration-on-mod-security)
  - [Future inspiration on whole stack hardening](#future-inspiration-on-whole-stack-hardening)
  - [Future inspiration on ansible](#future-inspiration-on-ansible)
  - [Inspiration on Readme.md](#inspiration-on-readme.md)
  - [Inspiration on Markdown Syntax](#inspiration-on-markdown syntax)

## todo
* remove select modules from 00-base.conf 
** and move them to the httpd-conf.sh 
** and remove the activation of the 00-base.conf file
* audit script
    * checks if rules are in configuration
    * does a functional check on the goal of the configuration changes.
    * Audit script has two functions per hardening step
    * 1. check the bad situation (as if the hardening step was not executed)
    * 2. check the good situation (as if the hardening step was executed successfull)

## TL;DR
`docker-compose build`

`docker-compose up`

`lynx localhost:80`
`lynx localhost:80/test` 

`docker-compose down`

## On Hardening
### Goal
The goal is to create a server that is default less vurnable for attacks.

Default apache has some settings that can be considered not ok for production.
Changing the configuration to fix settings that are considered to open are called **Hardening**.
In Security terms this is considered to be a **Blue team** responsibility. 

#### todo after hardening apache
1. Add SSL to apache and disable http trafic.
1. Add application firewall
2. Add hardening of the rest of the machine (os, firewall and other applications)
3. Add ecosystem hardening (DDOS protection, IDS, Firewalls)

#### Blue Team, Red Team, Purple team
* [Blue team](https://en.wikipedia.org/wiki/Blue_team_(computer_security)) (defense)
* [Red Team](https://en.wikipedia.org/wiki/Red_team) (offense)
* [Purple Team](https://danielmiessler.com/study/red-blue-purple-teams/)

### Approach to hardening / configuration management.
#### Reason for configuration management
1. you want to have insight what configuration is and should be, 
1. you want to have to be able to audit your configuration
1. you want to validate your configuration (what it should be vs what it is)
1. you want to be able to trace configuration changes
1. you want to be able to quickly recreate a configuration (=automation)

#### Approaches for configuration management
1. use the default provided configuration file and **change** it by search & replace
  1. Docker Specific 
    1. Do this in the Dockerfile
    1. Do this in a script during dockerbuild
  1. Do this after build with script
1. remove the default provided configuration file and **replace** it by a valideted configuration.
  1. Docker Specific 
    1. Do this in the Dockerfile
    1. Do this in a script during dockerbuild
  1. Do this after build with script
1. use a configuration management tool to keep configuration **in sync**

##### All options have in common
1. Support Version Controle (and thus audit trails)
1. Support automated deploymenet of configuration.
1. Version Control + Automated deployment => automated health and audit checks.

##### Option 1: Change default config files
###### Upside change
You begin with default that is always the same and sticking to the source (distro).
So you know by having a QA process that you are always in sync with the distro release cycle.

###### Downside change
1. when a distro is changing the config file, or something else changes this and then you should have testing to discover and lot of work.
1. when there is 1 config file then configuration management via search & replace is a bit troublesome. 
Best is to use various small config files. 
But then the search and replace is only possible as option when the distro supports it
1. this scripts usually only run during creation of the machine.

##### Option 2: Replace default config files
###### Upside replace
1. You always know what configuration is running
1. When the default configuration is compromised this is no issue because you remove it.
1. Replacing files is always faster then running many search and replace operations.
1. You can point responsibility (ownership) per configuration file.

###### Downside replace 
1. keeping audit/monitorng/health scripts in sync is difficult as discipline and difficult to validate.
1. this steps usually only work during creation of the machine.

##### Option 3: Keep configuration in sync with cm tooling
There are many options but these are the most popular. in this order. Ansible does not need an agent and therefor is very popular.
1. Ansible
1. Puppet
1. Chef

On Wikipedia there is a great [comparison table][configurationManagement01].
But do not forget that this type of software can not be selected by a checklist.
It should be tried and tested to fit your processes and culture.

[configurationManagement01]: https://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software

See also the inspiration links for some ansible (and docker) links.

###### Upside: 
1. central configuration management all systems are kept in sync

###### Downside: 
1. there is the need to login to the client system or have an agent running. This increases the attack surface. Fixing it during creation and then disallow access is better.

###### Summary
1. When provisioning on the fly configuration management is in the provisioning processes
    2. When using iron or machines that are running through multiple update cycles, you use Ansible.

### Advice for hardening
My advice would be the following:
1. Always
    1. start with changing the default configuration with a script
    1. mature as a group to find out specific configuration desires
        1. use the replace/add lines from step 1 as library for why things are changes
        2. create health/audit scripts that check every replace rule in the script for monitoring.
        3. Repeat: Use functional tests for every rule!
1. when building often new machines
    1. Splitup configuration in as many as possible small files
    1. when new stack is build
        1. delete all default config files
        2. replace with config files from repository
        3. run healt/audit scripts that check every config change that are created in the beginning
        4. when adding / changing configuration in the files, also fix the audit scripts (this needs discipline!)
1. when machines are running for a long time. 
    1. Look into a configuration management tool that keeps machines in sync.
    2. determine if running an agent is an option.
        3. No agent -> Ansible
        4. Agent -> add Cheff & Puppet to the comparison.

## Getting Started
This code will give you a docker container with the most recent CentOS, updated, apache base installed and configuration of apache changed by a script.

### Prerequisites
see Gitlab [centOS-Updated](https://gitlab.com/edzob/centOS-updated) for instructions and documentation
1. You need a machine with docker and it would help to also have docker-compose installed.
  1. Select place for running docker daemon
  1. Setup VM (gitlab-keys & docker & docker-compose)

### Run it
1. Login on your vm
1. go to the project dir for example 
`cd Projects`
1. clone the project repository 
`git clone https://gitlab.com/edzob/centOS-blueApache` 
1. go to the repository dir 
`cd centOs-blueApache`
1. build the container via docker compose. 
`docker-compose build`
1. run the container (spin up)
`docker-compose up`
1. Validate the Apache is running
`http://external-ip:80` 
1. Validate that the script has run successfull
`http://external-ip:80/test`    
1. Login to the container and look around.
`docker-compose run --rm centos-blueapache /bin/bash`

### Content of hardening
* Disable all Apache modules
* Enable the minimum Apache modules
* Global Apache configuration changes
* DDOS: Default Apache Timeout is 300 seconds set to 60
* Disable HTTP 1.0 Protocol
* Apache Header configuration
* Add logformat fields
* Disable Apache directory indexes
* Disable Apache Server Side Includes
* Disable Apache directory icons
* Disable Apache directory files
* Apache Configtest
* [robots.txt](http://www.robotstxt.org/) // [Wikipedia Robots exclusion standard](https://en.wikipedia.org/wiki/Robots_exclusion_standard)

It is good practice to add security txt to your website.   
It will not stop bad people/bots, but it will reduce exposure on the easy to search sources.

```
User-agent: *
Disallow: /
```
* [security.txt](https://securitytxt.org/) // [Security.txt GitHub Organization](https://github.com/securitytxt/security-txt)   

It is good practice to add security txt to your website.   
This ways people know who to contact.

```
# Our security address
Contact: mailto:security@example.com

# Our PGP key
Encryption: https://example.com/pgp-key.txt

# Our security policy
Policy: https://example.com/security-policy.html
```

## Built With
### Devices
* Chromebook
* Google Cloud Computing

### CodeEditor
* Chromebook
    * [Cloud9](https://ide.c9.io/) Code editor
* Google cloud
    * vi

### Programming Language(s)
* Bash
* Dockerfile
* YML

### Tools
* [Docker](https://docs.docker.com)
* [Docker-compose](https://docs.docker.com/compose)
* [Lynx](http://lynx.browser.org/)
    * Alternate : [elinks](http://elinks.or.cz/) 

## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](http://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/centOS-apache/graphs/master) who participated in this project.

## License
```
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.
```

## Acknowledgments
### Main inspiration on the script approach
* Great resposity of scripts to harden services (apache and more) by [Blue-Team][hardeningApache06]

### Inspiration on Apache Hardening Configuration rules
* Dockerfile CentOS 6 (apache & php) with nice hardning script lines by [Stefano Corallo][hardeningApache02]
* Dockerfile CentOS 7 by [CodeCloud][hardeningApache03]
* Great overview of hardening rules with explination why by [GeekFlare][hardeningApache04]
* Great collection of hardening configuration from [Horloge Skynet][hardeningApache11].

### Inspiration on HTTTP Headers
* list of some headers that need to be set in apache by [owasp][hardeningApache10]
* Great list of http headers by [FixMyWP][hardeningApache14]

### Future inspiration on mod_security
* focus on mod_security by [trustwave][hardeningApache07]

### Future inspiration on whole stack hardening
* full stack list not very clear if it is really good work by [Digital Ocean][hardeningApache12]
* full stack list not very clear if it is really good work by [nixCraft][hardeningApache13]
* a wiki on atttack fronts (redteam) by [bluscreenofjeff][hardeningApache08]

[hardeningApache06]: https://github.com/maldevel/blue-team

[hardeningApache02]: https://github.com/stefanorg/centos-php56/blob/master/Dockerfile
[hardeningApache03]: https://github.com/codecloud/docker-centos-apache-php7/blob/master/Dockerfile
[hardeningApache04]: https://geekflare.com/apache-web-server-hardening-security/
[hardeningApache11]: https://horlogeskynet.github.io/blog/security/hardening-apache-all-in-one-place

[hardeningApache14]: https://fixmywp.com/security/http-security-headers-hardening.php
[hardeningApache10]: https://www.owasp.org/index.php/Security_Headers
[hardeningApache07]: https://www.trustwave.com/Resources/SpiderLabs-Blog/Web-Application-Defender-s-Cookbook--CCDC-Blue-Team-Cheatsheet/

[hardeningApache12]: https://www.digitalocean.com/community/questions/best-practices-for-hardening-new-sever-in-2017
[hardeningApache13]: https://www.cyberciti.biz/tips/linux-security.html
[hardeningApache08]: https://github.com/bluscreenofjeff/Red-Team-Infrastructure-Wiki/blob/master/README.md#https

### Future inspiration on ansible
* List of Ansible rules for hardening apache machine (includes more then only apache configuration) by [JuJu4][hardeningApache01]
* "Managing Docker Containers with Ansible" by [Linux Academy, Rilindo F][ansibleDocker01]
* " How to Install and Configure Ansible on CentOS 7" by [Digital Ocean, Brian Hogan][ansibleCentOS01]

[hardeningApache01]: https://github.com/juju4/ansible-harden-apache

[ansibleDocker01]: https://linuxacademy.com/howtoguides/posts/show/topic/13750-managing-docker-containers-with-ansible
[ansibleCentOS01]: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-centos-7

### inspiration on bash file manupilation
* "[Rename all files in directory from $filename_h to $filename_half?][stack01]" by stackoverflow

[stack01]: https://stackoverflow.com/questions/7450818/rename-all-files-in-directory-from-filename-h-to-filename-half

### Inspiration on Readme.md
* Inspiration on Readme.md
  * Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
  * Github user jxson wrote: "[README.md template][readme02]"
  * Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

### Inspiration on Markdown Syntax
* Inspiration on Markdown Syntax
    * GitHub   
      * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
      * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
      * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
      * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
    * GitLab
      * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
      * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"

[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/