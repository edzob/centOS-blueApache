# Version 20180225
# Pull from CentOS RPM Build Image
FROM centos

MAINTAINER Edzo Botjes <e.a.botjes@gmail.com>

# Apache Environment Variables
ENV APACHE_DOC_ROOT "/var/www/html"
ENV APACHE_RUN_USER "apache"
ENV APACHE_RUN_GROUP "apache"
ENV APACHE_LOG_DIR "/var/log/httpd"
ENV APACHE_BASE_CONF_DIR "/etc/httpd/conf"
ENV APACHE_BASE_CONF_FILENAME "httpd.conf"
ENV APACHE_EXTRA_CONF_DIR "/etc/httpd/conf.d"
ENV APACHE_SSL_CONF_FILENAME "ssl.conf"
ENV APACHE_ServerName "localhost"
ENV APACHE_CONF_MOD_DIR "/etc/httpd/conf.modules.d"

# security.txt Enviroment Variables
ENV SECURITY_EMAIL "security@example.com"
ENV SECURITY_PGP_PUBLIC_KEY "https://example.com/pgp-key.txt"
ENV SECURITY_POLICY "https://example.com/security-policy.html"

# Update the image
RUN yum install --setopt=tsflags=nodocs -y deltarpm && \
    rpm --rebuilddb && yum clean all && \
    yum install --setopt=tsflags=nodocs -y epel-release && \
    yum update --setopt=tsflags=nodocs -y

# Use easy install to install supervisord
RUN yum --setopt=tsflags=nodocs -y install python-setuptools
RUN easy_install supervisor
RUN mkdir -p /var/log/supervisor
COPY conf/supervisord.conf /etc/supervisord.conf

# Some tool for when needed.
# RUN yum --setopt=tsflags=nodocs -y install which vi sed git lynx

# Install Apache    
RUN yum --setopt=tsflags=nodocs -y install httpd

# Get hardening script and run it?
COPY conf/httpd-conf.sh /tmp/httpd-conf.sh
RUN chmod 500 /tmp/httpd-conf.sh 
RUN /tmp/httpd-conf.sh

# robots.txt
RUN echo "User-agent: *" > $APACHE_DOC_ROOT/robots.txt && \
    echo "Disallow: /" >> $APACHE_DOC_ROOT/robots.txt

# security.txt
RUN echo "# Our security address" > $APACHE_DOC_ROOT/security.txt && \
    echo "Contact: mailto:$SECURITY_EMAIL" >> $APACHE_DOC_ROOT/security.txt && \
    echo "">> $APACHE_DOC_ROOT/security.txt && \
    echo "# Our PGP key" >> $APACHE_DOC_ROOT/security.txt && \
    echo "Encryption: $SECURITY_PGP_PUBLIC_KEY" >> $APACHE_DOC_ROOT/security.txt && \
    echo "" >> $APACHE_DOC_ROOT/security.txt && \
    echo "# Our security policy" >> $APACHE_DOC_ROOT/security.txt && \
    echo "Policy: $SECURITY_POLICY" >> $APACHE_DOC_ROOT/security.txt

# Content WebServer
RUN echo "<p>Hello Apache server on CentOS Docker </p>" >  $APACHE_DOC_ROOT/index.html && \
    echo "<p>$(cat /etc/centos-release) </p>" >> $APACHE_DOC_ROOT/index.html && \
    echo "<p>$(apachectl -v) </p>" >> $APACHE_DOC_ROOT/index.html
RUN mkdir  $APACHE_DOC_ROOT/test -p && touch  $APACHE_DOC_ROOT//test/hi && touch  $APACHE_DOC_ROOT//test/hello 

#cleanup
RUN yum clean all && rm -rf /var/cache/yum

EXPOSE 80 
CMD ["/usr/bin/supervisord"]
